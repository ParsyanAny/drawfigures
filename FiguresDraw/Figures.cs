using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
namespace FiguresDraw
{
    class Figures
    {
        #region Ctors
        public Figures() { }
        public Figures(byte length, char simbol)
        {
            Length1 = length;
            Simbol = simbol;
        }
        public Figures(byte length)
        {
            Length1 = length;
            Simbol = '-';
        }
        public Figures(char simbol)
        {
            Simbol = simbol;
            Length1 = 15;
        }
        public Figures(byte length1, byte length2, char simbol)
        {
            Length1 = length1;
            Length2 = length2;
            Simbol = simbol;
        }
        public Figures(byte length1, byte length2)
        {
            Length1 = length1;
            Length2 = length2;
            Simbol = '-';
        }
        #endregion
        public int S { get; set; }
        public int P { get; set; }

        private byte length1;
        public byte Length1
        {
            get { return length1; }
            set
            {
                if (value <= Console.WindowWidth)
                    length1 = value;
                else
                {
                    Console.WriteLine("Value is higher than Console's WindowWidth. \nThe max WindowWidth value is 120. \nPlease try again");

                }
            }
        }
        private byte length2;
        public byte Length2
        {
            get { return length2; }
            set
            {
                if (value <= Console.WindowWidth)
                    length2 = value;
                else
                {
                    Console.WriteLine("Value is higher than Console's WindowWidth. \nThe max WindowWidth value is 120. \nPlease try again");

                }
            }
        }
        private char simbol;
        public char Simbol
        {
            get { return simbol; }
            set
            {
                if (char.IsLetterOrDigit(value) || char.IsWhiteSpace(value))
                    value = '-';

                simbol = value;
            }
        }
        public virtual void Info()
        {
            var t = GetType().Name;
            ForegroundColor = ConsoleColor.Red;
            SetCursorPosition(49, 3);
            WriteLine($"This is your {t}!");
            ForegroundColor = ConsoleColor.Cyan;
            SetCursorPosition(47, 5);
            WriteLine($"Your {t}'s length = {Length1}");
            ResetColor();
        }
    }
}