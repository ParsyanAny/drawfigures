using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace FiguresDraw
{
    class Line : Figures
    {
        public Line() : base() { }
        public Line(byte length, char simbol) : base(length, simbol) { }
        public Line(byte length) : base(length) { }
        public Line(char simbol) : base(simbol) { }
            
            public virtual void Draw(Line li)
            {
                if (Length1 > 0)
                {
                    for (int i = 0; i < Length1; i++)
                    {
                        System.Threading.Thread.Sleep(55);
                        SetCursorPosition(60 - Length1/2 + i, 10);
                        Write(Simbol);
                    }
                }
            }
    }
}