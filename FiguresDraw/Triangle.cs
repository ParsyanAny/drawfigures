using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
namespace FiguresDraw
{
    class Triangle : Figures
    {
        #region ctors
        public Triangle() { }
        public Triangle(byte length1, byte length2, char simbol) : base(length1, length2, simbol) { }
        public Triangle(byte length1, byte length2) : base(length1,length2) { }
        public Triangle(byte length) : base(length) { Length2 = 15; }
        public Triangle(byte length, char simbol) : base(length, simbol) { Length2 = 15; }
        public Triangle(char simbol) : base(simbol) { Length1 = 15; Length2 = 15; }
        #endregion
        public void Draw(Triangle tri)
        {
            if (Length1 > 0 && Length2 > 0)
            {
                SetCursorPosition(60, 10);
                Write(Simbol);
                for (int i = 0; i < Length1; i++)
                {

                    SetCursorPosition(60 - i, 10 + i);
                    Write(Simbol);
                    SetCursorPosition(60 + i, 10 + i);
                    Write(Simbol);
                    System.Threading.Thread.Sleep(70);
                }
                SetCursorPosition(60 - Length1, 10 + Length1);
                for (int i = 0; i < Length2; i++)
                {
                    Write(Simbol + " ");
                    System.Threading.Thread.Sleep(70);
                }
            }
        }
        public override void Info()
        {
            P = Length1 * 2 + Length2;
            base.Info();

            ForegroundColor = ConsoleColor.DarkYellow;
            SetCursorPosition(46, 7);
            WriteLine($"A = B = {Length1}, C = {Length2}");
            ResetColor();
            ForegroundColor = ConsoleColor.Cyan;
            SetCursorPosition(46, 9);
            WriteLine($"P = {P}");
        }
    }

}