using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace FiguresDraw
{
    class Square : Figures
    {
        #region Ctors
        public Square() : base() {}
        public Square(byte length, char simbol) : base(length, simbol) { }
        public Square(byte length) : base(length) { }
        public Square(char simbol) : base(simbol) { }
        #endregion
        public void Draw(Square sq)
        {
            if (Length1 > 0)
            {
                for (int i = 0; i < Length1; i++)
                {
                    SetCursorPosition(60 - Length1 / 2 + i, 10);
                    Write(Simbol);
                    SetCursorPosition(60 - Length1 / 2 + i, 10 + Length1 / 2);
                    Write(Simbol);
                    System.Threading.Thread.Sleep(50);
                }
                for (int i = 0; i < Length1 / 2; i++)
                {
                    SetCursorPosition(60 - Length1 / 2, 10 + i);
                    Write(Simbol);
                    SetCursorPosition(60 - Length1 / 2 + Length1 - 1, 10 + i);
                    Write(Simbol);
                    System.Threading.Thread.Sleep(50);
                }
            }
        }
        public override void Info()
        {
            S = 4 * Length1;
            P = 4 * Length1;
            base.Info();
            ForegroundColor = ConsoleColor.Yellow;
            SetCursorPosition(60 - Length1 / 2, 9);
            WriteLine($"A = B = C = D = {Length1}");
            SetCursorPosition(60, 12);
            WriteLine($"S = {S}");
            SetCursorPosition(60, 11);
            WriteLine($"P = {P}");
        }
    }
}