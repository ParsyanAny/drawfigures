using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace FiguresDraw
{
    class Arrow :  Line
    {

        private char arrow { get; } = '>';
        public Arrow() : base () { { arrow = '>'; } }
        public Arrow(byte length, char simbol) : base(length, simbol) { arrow = '>'; }
        public Arrow(byte length) : base (length) { arrow = '>'; }
        public Arrow(char simbol) : base (simbol) { arrow = '>'; }
        public override void Draw(Line li)
        {
            base.Draw(li);
            WriteLine(arrow);
        }
    }
}