using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace FiguresDraw
{
    class Rectangle : Figures
    {
        #region Ctors
        public Rectangle() { }
        public Rectangle(byte length1, byte length2, char simbol) : base(length1, length2, simbol) { }
        public Rectangle(byte length1, byte length2) : base(length1, length2) {}
        public Rectangle(byte length) : base(length) { Length2 = 15; }
        public Rectangle(byte length, char simbol) : base(length, simbol) { Length2 = 15; }
        public Rectangle(char simbol) : base(simbol) { Length1 = 15; Length2 = 15; }
        #endregion
        public void Draw(Rectangle rec)
        {
            if (Length1 > 0 && Length2 > 0)
            {
                for (int i = 0; i < Length1; i++)
                {
                    SetCursorPosition(60 - Length1 / 2 + i, 10);
                    Write(Simbol);
                    SetCursorPosition(60 - Length1 / 2 + i, 10 + Length2 / 2);
                    Write(Simbol);
                    System.Threading.Thread.Sleep(50);
                }
                for (int i = 0; i < Length2 / 2; i++)
                {
                    SetCursorPosition(60 - Length1 / 2, 10 + i);
                    Write(Simbol);
                    SetCursorPosition(60 - Length1 / 2 + Length1 - 1, 10 + i);
                    Write(Simbol);
                    System.Threading.Thread.Sleep(50);
                }
            }
        }
        public override void Info()
        {
            S = Length1 * Length2;
            P = Length1 * 2 + Length2 * 2;
            base.Info();
            ForegroundColor = ConsoleColor.Yellow;
            SetCursorPosition(60 - Length1 / 2, 9);
            WriteLine($"A1 = {Length1}, A2 = {Length2}");
            SetCursorPosition(60, 12);
            WriteLine($"S = {S}");
            SetCursorPosition(60, 13);
            WriteLine($"P = {P}");
        }
    }
}